import request from "supertest"
import app from "../src/index.js"

let book = {name: "jksad", author: "sfsd", read: true}

describe("Index endpoints", () => {
    it("returns 200 when getting the root", (done) => {
        request(app)
        .get("/api/v1/books")
        .expect(200, done)
    })

    it("returns 201 when book is posted", (done) => {
        request(app)
        .post("/api/v1/books")
        .send(book)
        .expect(201, done)
    })

    it("returns 204 when book is deleted", (done) => {
        request(app)
        .delete("/api/v1/books/:id")
        .expect(204, done)
    })

    it("returns 200 when specific book is found", (done) => {
        request(app)
        .get("/api/v1/books/:id")
        .expect(200, done)
    })

    // it("returns 200 when book is updated", (done) => {
    //     const bookEdit = {name: "sdfsd", author: "dfsdf", read: true}
    //     request(app)
    //     .put("/api/v1/books/:id")
    //     .send(bookEdit)
    //     .expect(200, done)
    // })
})
