import express from "express";
import { v4 as uuidv4 } from 'uuid';

const router = express.Router();

let books = []

router.get("/", (req, res)=>{
    console.log(books);
    res.send(books);
});

router.post("/", (req, res)=>{
    const book = req.body;

    books.push({...book, id: uuidv4()});

    res.status(201).send(`book with the name ${book.name} added to database`);
});

router.get("/:id", (req, res)=>{
    const {id} = req.params;

    const foundBook = books.find((book)=> book.id === id)

    res.send(foundBook);
});

router.delete("/:id", (req, res)=>{
    const {id} = req.params;

    books = books.filter((book)=> book.id !== id)

    res.status(204).send(`Book with the id ${id} deleted from the database`);
});

router.put("/:id", (req, res)=>{
    const {id} = req.params;

    const bookUpdate = books.find((book) => book.id === id)

    bookUpdate.name = req.body.name;
    bookUpdate.author = req.body.author;
    bookUpdate.read = req.body.read;


    res.send(`Book with the id ${id} updated`);
});

export default router;