const createBooksTable = `
    CREATE TABLE IF NOT EXISTS "books" (
	    "id" UUID NOT NULL,
	    "name" VARCHAR(100) NOT NULL,
	    "author" VARCHAR(100) NOT NULL,
        "read" BOOLEAN NOT NULL,
	    PRIMARY KEY ("id")
    );`

    export default createBooksTable;
