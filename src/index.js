import express from "express";
import booksRouter from "./api/booksRouter.js";
import dotenv from "dotenv";
import { createTables } from './db/db.js'

process.env.NODE_ENV === 'dev' && dotenv.config()

const app = express();
const APP_PORT = process.env.APP_PORT || 8080

app.use(express.json());

app.use("/api/v1/books", booksRouter);

app.use((req, res, next) => {
    const err = new Error("not found");
    err.status = 404;
    next(err)
})

app.use((err, req, res, next) => {
    res.status(err.status || 500)
    res.send({
        error: {
            message: err.message
        }
    })
})

const environment = process.env.NODE_ENV;

environment !== "test" && createTables() && app.listen(APP_PORT, ()=>{
    console.log(`server running on port ${APP_PORT}`);
})

export default app