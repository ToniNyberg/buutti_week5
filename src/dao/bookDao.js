import { v4 as uuidv4 } from 'uuid'
import executeQuery from '../db/db.js'

const insertBook = async (book) => {
    const params = [uuidv4(), ...Object.values(book)]
    console.log(`Inserting a new product ${params[0]}...`)
    const result = await executeQuery(
        'INSERT INTO "products" ("id","name","price") VALUES ($1, $2, $3);', 
        params
    )
    console.log(`New book ${params[0]} inserted successfully.`)
    return result
}

const updateBook = async (book) => {
    const params = [book.name, book.author, book.id]
    console.log(`Updating a product ${params[0]}...`)
    const result = await executeQuery(
        'UPDATE "products" SET name=$1, author=$2 WHERE id=$3;', 
        params
    )
    console.log(`Book ${params[0]} updated successfully.`)
    return result
}

const findAll = async () => {
    console.log('Requesting for all books...')
    const result = await executeQuery(
        'SELECT * FROM "books";'
    )
    console.log(`Found ${result.rows.length} books.`)
    return result
}

const findOne = async (id) => {
    console.log(`Requesting a book with id: ${id}...`)
    const result = await executeQuery(
        'SELECT * FROM "books" WHERE id = $1;', [id]
    )
    console.log(`Found ${result.rows.length} books.`)
    if (result.rowCount !== 1) {
        const err = new Error('Internal error')
        err.name = 'dbError'
        throw err
    }
    return result.rows[0]
}

export default {
    insertBook,
    findAll,
    findOne,
    updateBook
}
