import bookDao from '../dao/bookDao.js'

const validateProduct = (book) => {
    return book['name'] && book['author']
}

const insertBook = async (book) => {
    if (!validateProduct(book)) {
        const err = new Error('Invalid product')
        err.name = 'UserError'
        throw err
    } else {
        return await bookDao.insertProduct(book)
    }
}

const updateBook = async (book) => {
    if (!validateProduct(book) || !book.id) {
        const err = new Error('Invalid product')
        err.name = 'UserError'
        throw err
    } else {
        return await bookDao.updateProduct(book)
    }
}

const findAll = async () => {
    const books = await bookDao.findAll()
    return books.rows
}

const findOne = async (id) => {
    const oneBook = await bookDao.findOne(id)
    return oneBook
}

export default { 
    findAll,
    findOne, 
    insertBook,
    updateBook
}
